#!/usr/bin/env ocaml
#use "topfind";;
#require "topkg"
open Topkg

let () =
	Pkg.describe "marxel" @@ fun c ->
	Ok [
		Pkg.mllib "src/marxel.mllib";
		Pkg.bin "src/marxel";
		Pkg.test "test/test_picker";
		Pkg.test "test/test_bits";
		Pkg.test "test/test_palettes";
	]
