
open Palettes

let pp_rgb fmt (c:Color.rgb) = Format.fprintf fmt "rgb(%d, %d, %d)" c.r c.g c.b
let pp_int = Format.pp_print_int

let tests_range = [
  (fun () -> assert (0 -- 0 = [0]));
  (fun () -> assert (0 -- 1 = [0;1]));
  (fun () -> assert (0 -- 2 = [0;1;2]));
  (fun () -> assert (1 -- 2 = [1;2]));
  (fun () -> assert (1 -- 10 = [1;2;3;4;5;6;7;8;9;10]));
  (fun () -> assert (10 -- 1 = []));
  (fun () -> assert (5 -- 4 = []));
]

let tests_refill = [
  (fun () -> assert (refill ~size:1 0b1 = 0b11111111));
  (fun () -> assert (refill ~size:1 0b0 = 0b0));
  (fun () -> assert (refill ~size:2 0b0 = 0b0));
  (fun () -> assert (refill ~size:2 0b1 = 0b1010101));
  (fun () -> assert (refill ~size:2 0b10 = 0b10101010));
  (fun () -> assert (refill ~size:2 0b11 = 0b11111111));
  (fun () -> assert (refill ~size:3 0b100 = 0b10010010));
  (fun () -> assert (refill ~size:3 0b101 = 0b10110110));
  (fun () -> assert (refill ~size:3 0b110 = 0b11011011));
  (fun () -> assert (refill ~size:6 0b110100 = 0b11010011));
  (fun () -> assert (refill ~size:7 0b1101101 = 0b11011011));
]

let rgb r g b = { Color.r; g; b}
let grey v = rgb v v v
let white = grey 255
let black = grey 0

let tests_bw = [
  (fun () -> assert (BW1.of_rgb black = 0b0));
  (fun () -> assert (BW1.to_rgb 0b0 = black));
  (fun () -> assert (BW1.of_rgb white = 0b1));
  (fun () -> assert (BW1.to_rgb 0b1 = white));
  (fun () -> assert (BW1.of_rgb (grey 0b10000000) = 0b1));
  (fun () -> assert (BW2.of_rgb black = 0b0));
  (fun () -> assert (BW2.to_rgb 0b0 = black));
  (fun () -> assert (BW2.of_rgb white = 0b11));
  (fun () -> assert (BW2.to_rgb 0b11 = white));
  (fun () -> assert (BW2.of_rgb (grey 0b10000000) = 0b10));
  (fun () -> assert (BW2.of_rgb (grey 0b10010100) = 0b10));
  (fun () -> assert (BW2.of_rgb (grey 0b10100110) = 0b10));
  (fun () -> assert (BW2.of_rgb (grey 0b10111111) = 0b10));
  (fun () -> assert (BW2.to_rgb 0b10 = grey 0b10101010));
]

let tests_rgb = [
  (fun () -> assert (RGB2.of_rgb black = 0b0));
  (fun () -> assert (RGB2.to_rgb 0b0 = black));
  (fun () -> assert (RGB2.of_rgb (rgb 255 255 0) = 0b1111));
  (fun () -> assert (RGB2.to_rgb 0b1111 = rgb 255 255 0));
  (fun () -> assert (RGB2.of_rgb white = 0b111111));
  (fun () -> assert (RGB2.to_rgb 0b111111 = white));
  (fun () -> assert (RGB2.of_rgb (grey 0b10111111) = 0b101010));
  (fun () -> assert (RGB2.to_rgb 0b101010 = grey 0b10101010));
]

let () =
  List.iter (fun t -> t ()) tests_range;
  List.iter (fun t -> t ()) tests_refill;
  List.iter (fun t -> t ()) tests_bw;
  List.iter (fun t -> t ()) tests_rgb;
  ()
