let eq ?(eq = (=)) ~loc ~pp a b =
  if eq a b then
    ()
  else
    Format.eprintf "%s: values ``%a'' and ``%a'' differ\n%!"
      loc
      pp a
      pp b
