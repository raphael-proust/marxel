
open Picker

let tests = [
  (fun () -> assert (
    (* weight 0, then 1 *)
    let p = init 0L false in
    let p = fold p 1L true in
    get p
    ));
  (fun () -> assert (
    (* weight 1, then 0 *)
    let p = init 1L true in
    let p = fold p 0L false in
    get p
    ));
  (fun () -> assert (
    (* This test has 1/10^6 chance of false positive *)
    let p = init 1L false in
    let rec more p n =
      if n <= 0 then
        p
      else
        more (fold p 1L true) (n - 1)
    in
    let p = more p 1_000_000 in
    get p
    ));
  (fun () -> assert (
    (* This test has 1/10^6 chance of false positive *)
    let p = init 1L true in
    let rec more p n =
      if n <= 0 then
        fold p 1L false
      else
        more (fold p 1L true) (n - 1)
    in
    let p = more p 1_000_000 in
    get p
  ));
]

let () = List.iter (fun t -> t ()) tests
