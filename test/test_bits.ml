
open Bits

let tests = [
  (fun () -> assert (popcount 0b0 = 0));
  (fun () -> assert (popcount 0b1 = 1));
  (fun () -> assert (popcount 0b10 = 1));
  (fun () -> assert (popcount 0b11 = 2));
  (fun () -> assert (popcount 0b1010 = 2));
  (fun () -> assert (popcount 0b11111111111111111 = 17));
  (fun () -> assert (popcount 0b10010100111000100001001111110111 = 17));

  (fun () -> assert (concat ~size:1 [0b0; 0b0; 0b0; 0b0; 0b0] = 0));
  (fun () -> assert (concat ~size:1 [0b1; 0b0; 0b0; 0b0; 0b0] = 0b1));
  (fun () -> assert (concat ~size:1 [0b0; 0b0; 0b0; 0b1; 0b0] = 0b1000));
  (fun () -> assert (concat ~size:1 [0b0; 0b0; 0b0; 0b0; 0b1] = 0b10000));
  (fun () -> assert (concat ~size:1 [0b0; 0b1; 0b0; 0b1; 0b0] = 0b1010));

  (fun () -> assert (concat ~size:4 [0xa; 0x1; 0x8; 0x8] = 0x881a));
  (fun () -> assert (concat ~size:8 [0xa3; 0x12; 0xf8] = 0xf812a3));

  (fun () -> assert (repeat ~count:30 ~size:1 0b0 = 0b0));
  (fun () -> assert (repeat ~count:8 ~size:1 0b1 = 0b11111111));
  (fun () -> assert (repeat ~count:4 ~size:4 0xa = 0xaaaa));
  (fun () -> assert (repeat ~count:8 ~size:4 0x6 = 0x66666666));
  (fun () -> assert (repeat ~count:2 ~size:8 0xf3 = 0xf3f3));
]

let () = List.iter (fun t -> t ()) tests
