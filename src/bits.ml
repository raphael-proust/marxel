(* Bit twiddling and mangling code *)

let popcount p =
  (* from http://gallium.inria.fr/blog/implementing-hamt-for-ocaml/ *)
  let sk5 = 0x55555555 in
  let sk3 = 0x33333333 in
  let skf0 = 0xf0f0f0f in
  (*     let skff = 0xff00ff in *)
  let p = p - (p lsr 1) land sk5 in
  let p = p land sk3 + (p lsr 2) land sk3 in
  let p = p land skf0 + (p lsr 4) land skf0 in
  let p = p + p lsr 8 in
  (p + p lsr 16) land 0x3f

let concat ~size values =
  let rec concat acc shift = function
    | [] -> acc
    | v::vs -> concat (acc lor (v lsl shift)) (shift + size) vs
  in
  concat 0 0 values

let repeat ~count ~size pat =
  let rec repeat count shift acc =
    if count = 0 then
      acc
    else
      repeat (pred count) (shift + size) (acc lor (pat lsl shift))
  in
  if count < 0 then
    raise (Invalid_argument "marxel.Bits.repeat")
  else
    repeat count 0 0
