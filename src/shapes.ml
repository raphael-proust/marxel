module type SHAPE = sig
  val name: string
  val descr: string

  val coordinates: (int * int) list
end

let margins_of_coordinates coordinates =
  List.fold_left
    (fun (t, l, b) (x, y) ->
      if x >=0 then
        failwith "TODO: support wider range of patterns"
      else
        let t = max (abs (max 0 y)) t in
        let l = max (abs x) l in
        let b = max (abs (min 0 y)) b in
        (t, l, b)
    )
    (0, 0, 0)
    coordinates

module L3: SHAPE = struct
  let name = "line3"
  let descr = "three pixels horizontally aligned"
  let coordinates = [ (~- 1, 0); (~- 2, 0); (~- 3, 0) ]
end

module S4: SHAPE = struct
  let name = "square4"
  let descr = "four pixels in a square"
  let coordinates = [ (~-2, ~-1); (~-1, ~-1); (~-2, 0); (~-1, 0); ]
end

module T5: SHAPE = struct
  let name = "tee5"
  let descr = "five pixels in a T-shape"
  let coordinates = [ (~-1, 1); (~-1, 0); (~-2, 0); (~-3, 0); (~-1, ~-1); ]
end

module A10: SHAPE = struct
  let name = "arrow10"
  let descr = "ten pixels in an arrow pattern"
  let coordinates = [
    (~- 1, 0); (~- 2, 0); (~- 3, 0); (~- 4, 0); (~- 5, 0); (~- 6, 0);
    (~- 1, 1); (~- 1, ~-1);
    (~- 2, 2); (~- 2, ~-2);
  ]
end

module D20: SHAPE = struct
  let name = "drop20"
  let descr = "twenty pixels in a water-drop shape"
  let coordinates = [
    (~-1, 2);
    (~-5, 1); (~-4, 1); (~-3, 1); (~-2, 1); (~-1, 1);
    (~-8, 0); (~-7, 0); (~-6, 0); (~-5, 0); (~-4, 0); (~-3, 0); (~-2, 0); (~-1, 0);
    (~-5, ~-1); (~-4, ~-1); (~-3, ~-1); (~-2, ~-1); (~-1, ~-1);
    (~-2, ~-2);
  ]
end

module T30: SHAPE = struct
  let name = "block30"
  let descr = "thrity pixels in a triangle-ish shape"
  let coordinates = [
                                            (~-4, 4); (~-3, 4); (~-2, 4); (~-1, 4);
                                  (~-5, 3); (~-4, 3); (~-3, 3); (~-2, 3); (~-1, 3);
                        (~-6, 2); (~-5, 2); (~-4, 2); (~-3, 2); (~-2, 2); (~-1, 2);
              (~-7, 1); (~-6, 1); (~-5, 1); (~-4, 1); (~-3, 1); (~-2, 1); (~-1, 1);
    (~-8, 0); (~-7, 0); (~-6, 0); (~-5, 0); (~-4, 0); (~-3, 0); (~-2, 0); (~-1, 0);
  ]
end

let default_shape = (module T5: SHAPE)
let shapes = [
  (module L3: SHAPE);
  (module S4: SHAPE);
  (module T5: SHAPE);
  (module A10: SHAPE);
  (module D20: SHAPE);
  (module T30: SHAPE);
]
