(* Helper functions for output of progress bar *)
let print_dot () = if Logs.level () >= Some Info then Format.printf ".%!"
let print_nl () = if Logs.level () >= Some Info then Format.printf "\n%!"

let main
  pattern_name input_names output_name
  palette_name shape_name weigher_name
  verbose logs_style =

  (* SETUP ARGUMENTS *)
  Fmt_tty.setup_std_outputs ?style_renderer:logs_style ();
  Logs.set_level verbose;
  Logs.set_reporter (Logs_fmt.reporter ());
  Logs.info (fun m -> m "Starting%!");
  Logs.debug (fun m -> m "Input files: %a; Pattern file: %s%!"
    (Format.pp_print_list ~pp_sep:Format.pp_print_space Format.pp_print_string)
    input_names
    pattern_name
    );
  let output_name = match output_name with
    | Some o -> o
    | None ->
      let o =
        let open Filename in
        remove_extension pattern_name ^ "-marxel" ^ extension pattern_name
      in
      Logs.info (fun m -> m "Output file unspecified, choosing %s%!" o);
      o
  in
  let palette =
    let palette =
      List.find_opt
        (fun (module P: Palettes.PALETTE) -> P.name = palette_name)
        Palettes.palettes
    in
    match palette with
    | Some p -> p
    | None -> Logs.err (fun m -> m "Invalid palette name"); exit 2
  in
  let shape =
    let shape =
      List.find_opt
        (fun (module S: Shapes.SHAPE) -> S.name = shape_name)
        Shapes.shapes
    in
    match shape with
    | Some s -> s
    | None -> Logs.err (fun m -> m "Invalid shape name"); exit 2
  in
  let weigher =
    let weigher =
      List.find_opt
        (fun (module W: Weighers.WEIGHER) -> W.name = weigher_name)
        Weighers.weighers
    in
    match weigher with
    | Some w -> w
    | None -> Logs.err (fun m -> m "Invalid weigher name"); exit 2
  in

  let module Palette = (val palette: Palettes.PALETTE) in
  let module Shape = (val shape: Shapes.SHAPE) in
  let module Weigher = (val weigher: Weighers.WEIGHER) in
  let module Pattern = Patterns.Make (Palette) (Shape) (Weigher) in
  let module Model = Models.Make (Palette) (Pattern) in

  (* CREATE MODEL *)
  let model = Model.init () in
  List.iter
    (fun input_name ->
      Logs.info (fun m -> m "Loading input file: %s%!" input_name);
      let input =
        match Images.load input_name [] with
        | Rgb24 image -> image
        | _ -> failwith "TODO: support more image types"
      in
      Pattern.collect ~f:(Model.give model) input
    )
    input_names;


  (* APPLY MODEL*)
  Logs.info (fun m -> m "Loading pattern file: %s%!" pattern_name);
  let canvas =
    match Images.load pattern_name [] with
        | Rgb24 image -> image
        | _ -> failwith "TODO: support more image types"
      in
  Logs.info (fun m -> m "Creating image%!");
  Pattern.distribute ~percent:print_dot ~f:(Model.take model) canvas;
  print_nl ();

  (* SAVING RESULT *)
  Logs.info (fun m -> m "Saving result in %s%!" output_name);
  Images.save output_name (Some Png) [Save_Quality 95] (Rgb24 canvas)



open Cmdliner

let pattern_name =
  let doc =
    "A required file to use as a starting-point and canvas to draw on. The \
    left, top, and bottom margins are unchanged (as required by the selected \
    SHAPE) and the rest is filled using the build model."
  in
  Arg.(required & pos 0 (some string) None & info [] ~docv:"PATTERN" ~doc)

let output_name =
  let doc =
    "The file name to save the result to. If absent, a name is generated based \
    on the name of the PATTERN file."
  in
  Arg.(value & opt (some string) None & info ["o"; "output"] ~docv:"OUTPUT" ~doc)

let input_names =
  let doc =
    "A non-empty list of files that are used as inputs. These are scanned in \
    order to build a model." in
  Arg.(non_empty & pos_right 0 string [] & info [] ~docv:"INPUT" ~doc)

let palette =
  let module Default = (val Palettes.default_palette) in
  let doc = "Select the palette to use" in
  let env = Arg.env_var "MARXEL_PALETTE" ~doc in
  let doc =
    Format.fprintf
      Format.str_formatter
      "Select the pallete to use. Palettes are simplified color spaces. Valid \
      values are %a."
      (Format.pp_print_list
        ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
        (fun fmt (module P: Palettes.PALETTE) ->
          if P.name = Default.name then
            Format.fprintf fmt "'%s' (default) (%s)" P.name P.descr
          else
            Format.fprintf fmt "'%s' (%s)" P.name P.descr
        )
      )
      Palettes.palettes;
      Format.flush_str_formatter ()
  in
  Arg.(value
    & opt string Default.name
    & info ["p"; "palette"] ~env ~docv:"PALETTE" ~doc)

let shape =
  let module Default = (val Shapes.default_shape) in
  let doc = "Select the shape to use" in
  let env = Arg.env_var "MARXEL_SHAPE" ~doc in
  let doc =
    Format.fprintf
      Format.str_formatter
      "Select the shape to use. Shapes are patterns of pixels that are \
      considered when building the model and generating the image. Valid \
      values are %a."
      (Format.pp_print_list
        ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
        (fun fmt (module S: Shapes.SHAPE) ->
          if S.name = Default.name then
            Format.fprintf fmt "'%s' (default) (%s)" S.name S.descr
          else
            Format.fprintf fmt "'%s' (%s)" S.name S.descr
        )
      )
      Shapes.shapes;
      Format.flush_str_formatter ()
  in
  Arg.(value
    & opt string Default.name
    & info ["s"; "shape"] ~env ~docv:"SHAPE" ~doc)

let weigher =
  let module Default = (val Weighers.default_weigher) in
  let doc = "Select the weighing function to use" in
  let env = Arg.env_var "MARXEL_WEIGHER" ~doc in
  let doc =
    Format.fprintf
      Format.str_formatter
      "Select the weighing function to use. Weighing functions are way to \
      agregate tiered similarity information (where the tiers correspond to \
      successive bit significance) into a single similarity measure. Valid \
      values are %a."
      (Format.pp_print_list
        ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
        (fun fmt (module W: Weighers.WEIGHER) ->
          if W.name = Default.name then
            Format.fprintf fmt "'%s' (default) (%s)" W.name W.descr
          else
            Format.fprintf fmt "'%s' (%s)" W.name W.descr
        )
      )
      Weighers.weighers;
      Format.flush_str_formatter ()
  in
  Arg.(value
    & opt string Default.name
    & info ["w"; "weigher"] ~env ~docv:"WEIGHER" ~doc)

let verbose = Logs_cli.level ()
let logs_style = Fmt_cli.style_renderer ()


let information =
  let doc = "Produce an image based on a model built from other images." in
  let man = [] in
  Term.info "marxel" ~version:"%%VERSION%%" ~doc ~exits:Term.default_exits ~man
let command =
  Term.(const main
    $ pattern_name $ input_names $ output_name
    $ palette $ shape $ weigher
    $ verbose $ logs_style)

let () = Term.(exit @@ eval (command, information))
