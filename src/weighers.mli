
module type WEIGHER = sig
  val name: string
  val descr: string
  val weigh: int list -> int
end

val default_weigher: (module WEIGHER)
val weighers: (module WEIGHER) list
