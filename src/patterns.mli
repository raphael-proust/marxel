module type PATTERN = sig
  type palette_t
  type t
  include Hashtbl.HashedType with type t := t
  val similarity: t -> t -> int
  val collect: f:(t -> palette_t -> unit) -> Rgb24.t -> unit
  val distribute: ?percent:(unit -> unit) -> f:(t -> palette_t) -> Rgb24.t -> unit
end

module Make
  (Palette: Palettes.PALETTE)
  (Shape: Shapes.SHAPE)
  (Weigher: Weighers.WEIGHER):
    PATTERN
      with type palette_t = Palette.t
