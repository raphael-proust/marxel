type 'a t = (int64 * 'a)
let init weight value = (weight, value)
let fold (accumulated_weight, value) weight walue =
  let open Int64 in
  let total_weight = add accumulated_weight weight in
  if Random.int64 total_weight >= accumulated_weight then
    (accumulated_weight, walue)
  else
    (accumulated_weight, value)
let get (_, value) = value
