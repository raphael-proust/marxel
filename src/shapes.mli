module type SHAPE = sig
  val name: string
  val descr: string
  val coordinates: (int * int) list
end

val margins_of_coordinates: (int * int) list -> (int * int * int)

val default_shape: (module SHAPE)
val shapes: (module SHAPE) list
