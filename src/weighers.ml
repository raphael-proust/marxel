
module type WEIGHER = sig
  val name: string
  val descr: string
  val weigh: int list -> int
end

module MakeFactor(M: sig val factor: int end): WEIGHER = struct
  let name = Format.sprintf "factor%d" M.factor
  let descr = Format.sprintf "each tier weighs %d times as much as the next" M.factor
  let weigh weights =
    List.fold_left (fun acc count -> (M.factor * acc) + count) 0 weights
end

module Factor1 = MakeFactor(struct let factor = 1 end)
module Factor2 = MakeFactor(struct let factor = 2 end)
module Factor4 = MakeFactor(struct let factor = 4 end)
module Factor8 = MakeFactor(struct let factor = 8 end)
module Factor16 = MakeFactor(struct let factor = 16 end)

let default_weigher = (module Factor8: WEIGHER)
let weighers = [
  (module Factor1: WEIGHER);
  (module Factor2: WEIGHER);
  (module Factor4: WEIGHER);
  (module Factor8: WEIGHER);
  (module Factor16: WEIGHER);
]


