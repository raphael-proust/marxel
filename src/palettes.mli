module type PALETTE = sig

  val name: string
  val descr: string

  type t = int
  val default: t
  val of_rgb: Color.rgb -> t
  val to_rgb: t -> Color.rgb

  val size: int
  val tier_patterns: int list

  include Hashtbl.HashedType with type t := t
end

val default_palette: (module PALETTE)
val palettes: (module PALETTE) list

(*/*)
(* For testing *)
val ( -- ): int -> int -> int list
val refill: size:int -> int -> int
module RGB2: PALETTE
module RGB3: PALETTE
module BW1: PALETTE
module BW2: PALETTE
module BW3: PALETTE
module BW4: PALETTE
