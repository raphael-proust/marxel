module type MODEL = sig
  type palette_t
  type pattern_t
  type t
  val init: unit -> t
  val give: t -> pattern_t -> palette_t -> unit
  val take: t -> pattern_t -> palette_t
end

module Make
  (Palette: Palettes.PALETTE)
  (Pattern: Patterns.PATTERN with type palette_t = Palette.t)
  : MODEL with type palette_t := Palette.t and type pattern_t := Pattern.t
  = struct
  module CH = Hashtbl.Make(Palette)
  module PH = Hashtbl.Make(Pattern)

  type t = (int * (int CH.t)) PH.t

  let init () : t = PH.create (64 * 64 * 64)
  let give (t:t) (p:Pattern.t) (c:Palette.t) =
    match PH.find_opt t p with
    | None ->
        let ch = CH.create 32 in
        CH.add ch c 1;
        PH.add t p (1, ch)
    | Some (i, ch) -> begin begin
      match CH.find_opt ch c with
        | None -> CH.add ch c 1
        | Some i -> CH.replace ch c (succ i)
      end;
      PH.replace t p (succ i, ch)
    end

  let take_ch (ch: int CH.t) : Palette.t =
    let c_picker =
      CH.fold
      (fun c i picker -> Picker.fold picker (Int64.of_int i) c)
      ch
      (Picker.init 1L Palette.default)
    in
    Picker.get c_picker

  let default_ch: (int * int CH.t) =
    let ch = CH.create 1 in
    CH.add ch Palette.default 1;
    (1, ch)

  let take_ph (t:t) (p:Pattern.t): (int * int CH.t) =
    let ch_picker =
      PH.fold
        (fun pp ((i, ch) as ich) picker ->
          let weight = Int64.of_int (i * (Pattern.similarity p pp)) in
          Picker.fold picker weight ich)
        t
        (Picker.init 1L default_ch)
    in
    Picker.get ch_picker

  let take t p =
    let (_, ch) = take_ph t p in
    take_ch ch

end
