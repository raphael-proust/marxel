type 'a t
val init: int64 -> 'a -> 'a t
val fold: 'a t -> int64 -> 'a -> 'a t
val get: 'a t -> 'a
