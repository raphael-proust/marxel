module type PALETTE = sig

  val name: string
  val descr: string

  type t = int
  val default: t
  val of_rgb: Color.rgb -> t
  val to_rgb: t -> Color.rgb

  val size: int
  val tier_patterns: int list

  include Hashtbl.HashedType with type t := t
end

let name_of_int = function
  | 1 -> "one"
  | 2 -> "two"
  | 3 -> "three"
  | 4 -> "four"
  | 5 -> "five"
  | 6 -> "six"
  | 7 -> "seven"
  | 8 -> "eight"
  | _ -> raise (Invalid_argument "marxel.Palettes.name_of_int")

let ( -- ) x y =
  let rec loop acc n =
    if n > y then
      acc
    else
      loop (n :: acc) (succ n)
  in
  List.rev (loop [] x)

let refill ~size value =
  (* Repeating the simple bit patterns ensures that [0b1\{size}] fills to
     [0b1\{8}]. It casts back the simplified patterns to more spread out
     values in the 0-255 range.  *)

  let count = 8 / size in
  let leftover = 8 mod size in
  let repeated = Bits.repeat ~count ~size value in
  let byte_aligned = repeated lsl leftover in

  let tail = value lsr (size - leftover) in
  let filled = byte_aligned lor tail in

  filled

module MakeRGB(M: sig val bits: int end): PALETTE = struct
  let name = Format.sprintf "RGB%d" M.bits
  let descr =
    Format.sprintf
      "%s bit%s for each of the R, G, and B components"
      (name_of_int M.bits)
      (if M.bits <> 1 then "s" else "")

  type t = int (* b\{bits}g\{bits}r\{bits} *)
  let (>>) n places =
    if places >= 0 then
      n lsr places
    else
      n lsl ~-places
  let (<<) n places =
    if places >= 0 then
      n lsl places
    else
      n lsr ~-places
  let component_mask = Bits.repeat ~count:M.bits ~size:1 0b1
  let significant = component_mask lsl (8 - M.bits)
  let r_mask = component_mask
  let g_mask = component_mask lsl M.bits
  let b_mask = component_mask lsl (2 * M.bits)
  let of_rgb (c:Color.rgb) =
    ((c.r land significant) >> (8 - M.bits)) lor
    ((c.g land significant) >> (8 - 2 * M.bits)) lor
    ((c.b land significant) >> (8 - 3 * M.bits))
  let to_rgb t =
    let r = refill ~size:M.bits (t land r_mask) in
    let g = refill ~size:M.bits ((t land g_mask) lsr M.bits) in
    let b = refill ~size:M.bits ((t land b_mask) lsr (2 * M.bits)) in
    {Color.r; g; b}
  let default = of_rgb {Color.r=128; g=128; b=128;}
  let size = M.bits * 3
  let tier_patterns =
    List.rev_map
      (fun i -> Bits.repeat ~count:3 ~size:M.bits (1 lsl (i-1)))
      (1 -- M.bits)
  let equal: t -> t -> bool = (=)
  let hash: t -> int = Hashtbl.hash
end

module RGB2 = MakeRGB(struct let bits = 2 end)
module RGB3 = MakeRGB(struct let bits = 3 end)

module MakeGrey(M: sig val bits: int end): PALETTE = struct
  let name = Format.sprintf "B&W%d" M.bits
  let descr =
    Format.sprintf
      "%s bit%s of greyscale"
      (name_of_int M.bits)
      (if M.bits <> 1 then "s" else "")
  type t = int
  let of_rgb (c:Color.rgb) =
    let grey = (c.r + c.g + c.b) / 3 in
    grey lsr (8 - M.bits)
  let to_rgb t =
    let v = refill ~size:M.bits t in
    {Color.r = v; g = v; b = v;}
  (* TODO: argument for default color *)
  let default = of_rgb {Color.r=128; g=128; b=128;}
  let size = M.bits
  let tier_patterns = List.rev_map (fun i -> 1 lsl (i - 1)) (1 -- M.bits)
  let equal: t -> t -> bool = (=)
  let hash: t -> int = Hashtbl.hash
end

module BW1: PALETTE = MakeGrey(struct let bits = 1 end)
module BW2: PALETTE = MakeGrey(struct let bits = 2 end)
module BW3: PALETTE = MakeGrey(struct let bits = 3 end)
module BW4: PALETTE = MakeGrey(struct let bits = 4 end)

let default_palette = (module RGB3: PALETTE)
let palettes: (module PALETTE) list = [
  (module RGB2: PALETTE);
  (module RGB3: PALETTE);
  (module BW1: PALETTE);
  (module BW2: PALETTE);
  (module BW3: PALETTE);
  (module BW4: PALETTE);
]
