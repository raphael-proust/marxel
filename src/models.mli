module type MODEL = sig
  type palette_t
  type pattern_t
  type t
  val init: unit -> t
  val give: t -> pattern_t -> palette_t -> unit
  val take: t -> pattern_t -> palette_t
end

module Make
  (Palette: Palettes.PALETTE)
  (Pattern: Patterns.PATTERN with type palette_t = Palette.t):
  MODEL
    with type pattern_t := Pattern.t
    and type palette_t := Palette.t
