let src = Logs.Src.create "marxell.Patterns"

module type PATTERN = sig
  type palette_t
  type t
  include Hashtbl.HashedType with type t := t
  val similarity: t -> t -> int
  val collect: f:(t -> palette_t -> unit) -> Rgb24.t -> unit
  val distribute: ?percent:(unit -> unit) -> f:(t -> palette_t) -> Rgb24.t -> unit
end

module Make
  (Palette: Palettes.PALETTE)
  (Shape: Shapes.SHAPE)
  (Weigher: Weighers.WEIGHER)
  : PATTERN with type palette_t = Palette.t
  = struct

  (* Sanity check *)
  let () =
    if Palette.size * List.length Shape.coordinates > Sys.word_size - 2 then begin
      Logs.err ~src (fun m -> m "Model chunks too big");
      exit 2
    end

  (* Setting up derived constants *)
  let coordinate_count = List.length Shape.coordinates
  let mask =
    Bits.repeat
      ~count:(Palette.size * coordinate_count)
      ~size:1
      0b1
  let (top_margin, left_margin, bottom_margin) =
    Shapes.margins_of_coordinates Shape.coordinates

  type palette_t = Palette.t
  type t = int

  let equal : t -> t -> bool = (=)
  let hash : t -> int = Hashtbl.hash

  let read_unsafe image ~x ~y =
    let pixels =
      List.map
        (fun (xo, yo) -> Palette.of_rgb @@ Rgb24.get image (x + xo) (y + yo))
        Shape.coordinates
    in
    Bits.concat Palette.size pixels

  let similarity p1 p2 =
    let bits = (p1 lxor p2) lxor mask in
    Weigher.weigh
      (List.map
        (fun pat -> Bits.popcount (bits land pat))
        Palette.tier_patterns)

  let collect ~f (image: Rgb24.t) =
    Logs.debug ~src (fun m -> m "Collecting patterns (%d, %d)%!" image.width image.height);
    let rec loop_x x y =
      if x >= image.width then
        ()
      else begin
        let pattern = read_unsafe image ~x ~y in
        let color = Palette.of_rgb @@ Rgb24.get image x y in
        f pattern color;
        loop_x (x + 1) y
      end
    in
    let rec loop_y y =
      if y >= image.height - top_margin then
        ()
      else begin
        loop_x left_margin y;
        loop_y (y + 1)
      end
    in
    loop_y bottom_margin

  let rec repeat n  f =
    if n <= 0 then
      ()
    else begin
      f ();
      repeat (pred n) f
    end

  let distribute ?(percent = fun () -> ()) ~f (image: Rgb24.t) =
    let rec loop_x x y =
      if x >= image.width then
        ()
      else begin
        let color = Palette.to_rgb @@ f (read_unsafe image ~x ~y) in
        Rgb24.set image x y color;
        loop_x (x + 1) y
      end
    in
    let rec loop_y pct y =
      let new_pct = (y * 100) / image.height in
      repeat (new_pct - pct) percent;
      if y >= image.height - top_margin then
        ()
      else begin
        loop_x left_margin y;
        loop_y new_pct (y + 1)
      end
    in
    loop_y 0 bottom_margin

end
